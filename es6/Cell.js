export default class Cell {
  constructor(
    theCheckTint, theRowNo, theColNo, theNosRow, theNosCol,
    theGridX, theGridY, theGridWidth, theGridHeight
  ){
    // Workaround for JS context peculiarities.
    // var self = this;
    this.checkTint = theCheckTint // Handles b/w of keyboard notes.
    this.rowNo = theRowNo
    this.colNo = theColNo
    this.nosRow = theNosRow
    this.nosCol = theNosCol
    this.gridX = theGridX
    this.gridY = theGridY
    this.gridWidth = theGridWidth
    this.gridHeight = theGridHeight
    this.x = this.gridX + this.colNo/this.nosCol*this.gridWidth
    this.y = this.gridY + this.rowNo/this.nosRow*this.gridHeight
    this.w = this.gridWidth/this.nosCol
    this.h = this.gridHeight/this.nosRow
    this.text = ""
    this.type = null
    this.startVal = null
    this.sf = null
    // Possible to return something.
    // return sth;
  }

  add_text(type, startVal, sf, mnns){
    if (this.type == null){
      this.type = type
    }
    if (this.startVal == null){
      this.startVal = startVal
    }
    if (this.sf == null){
      this.sf = sf
    }
    if (type == "pitch"){
      const mnn = mnns[startVal - this.rowNo]
      this.text = mu.mnn2pitch_simple(mnn)
    }
    else if (type == "ontime"){
      const val = sf*(startVal + this.colNo)
      if (Math.floor(val) !== val){
        this.text = ""
      }
      else {
        this.text = "" + val
      }
      // this.text = "" + Math.round(10*parseFloat(sf)*(startVal + this.colNo))/10
    }
    p.textAlign(p.CENTER, p.CENTER)
    p.textSize(10)
    p.noStroke()
    p.fill(50)
    p.text(this.text, this.x + this.w/2, this.y + this.h/2)
  }

  draw(rd, highlight, mnns, topMnn){
    if (rd){
      this.gridX = rd.gridX
      this.gridY = rd.gridY
      this.gridWidth = rd.gridWidth
      this.gridHeight = rd.gridHeight
      this.x = this.gridX + this.colNo/this.nosCol*this.gridWidth
      this.y = this.gridY + this.rowNo/this.nosRow*this.gridHeight
      this.w = this.gridWidth/this.nosCol
      this.h = this.gridHeight/this.nosRow
    }
    p.strokeWeight(0.5)
    p.stroke(150)
    p.rectMode(p.CORNER)
    if (highlight){
      p.fill(190, 250, 190)
    }
    else {
      p.fill(230, 230, 230)
    }
    // p.imageMode(p.CORNER)
    if (this.checkTint){
      const mnn = mnns[topMnn - this.rowNo]
      if ([0, 2, 4, 5, 7, 9, 11].indexOf(mnn % 12) >= 0){
        // White key on piano keyboard.
        p.fill(230, 230, 230, 50)
        // p.tint(255, 80)
      }
      else {
        // Black key on piano keyboard.
        p.fill(90, 90, 90, 50)
        // p.tint(255, 200)
      }
    }
    p.rect(
      this.x + 0.05*this.w, this.y + 0.05*this.h, 0.9*this.w, 0.9*this.h, 3
    )
    // p.image(this.bgnImg, this.x + 2, this.y + 2, 5, this.h - 4)
    // p.image(this.midImg, this.x + 7, this.y + 2, this.w - 14, this.h - 4)
    // p.image(this.endImg, this.x + this.w - 7, this.y + 2, 5, this.h - 4)
    // p.noTint()
  }

  set_background(str){
    if (str == "highlight"){
      this.draw(null, true)
    }
    else {
      this.draw()
    }
    this.add_text()
  }
  // set_background(bgnImg, midImg, endImg){
  //   this.bgnImg = bgnImg, this.midImg = midImg, this.endImg = endImg
  //   this.draw()
  //   this.add_text()
  // },

  touch_check(){
    if (
      p.mouseX > this.x && p.mouseX < this.x + this.w &&
      p.mouseY > this.y && p.mouseY < this.y + this.h
    ){
      if (prm.printConsoleLogs) { console.log("In cell row " + this.rowNo + ", col " + this.colNo + ".") }
      return { rowNo: this.rowNo, colNo: this.colNo }
    }
  }
}
