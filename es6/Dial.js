export default class Dial {
  constructor(_id, _x, _y, _radius, _min = 0, _max = 1, _val = 0.5, _step = null){
    this.id = _id
    this.x = _x
    this.y = _y
    this.radius = _radius
    this.min = _min
    this.max = _max
    this.val = _val
    this.step = _step
    this.gradations = null
    if (this.step !== null){
      let n = Math.floor((this.max - this.min)/this.step) + 1
      this.gradations = new Array(n)
      for (let i = 0; i < n; i++){
        this.gradations[i] = this.min + this.step*i
      }
    }
    // console.log("this.gradations:", this.gradations)

    // Pairing with Tone.js
    // this.toneObj = null
    // this.toneObjProperty = null

    // Defaults
    p.colorMode(p.RGB, 255)
    this.bgCol = p.color(50, 55, 100)
    p.colorMode(p.HSB, 100)
    this.fgCol = p.color(50, 55, 100)
    this.moving = false
  }


  draw(){
    p.strokeWeight(3)
    p.stroke(this.fgCol)
    p.fill(this.bgCol)
    p.circle(this.x, this.y, 2*this.radius)
    p.stroke(100)
    p.line(this.x, this.y, this.x, this.y + this.radius)
    p.stroke(this.fgCol)
    const prop = (this.val - this.min)/(this.max - this.min)
    p.line(
      this.x,
      this.y,
      this.x + this.radius*p.cos(p.TWO_PI*prop - p.HALF_PI),
      this.y - this.radius*p.sin(p.TWO_PI*prop - p.HALF_PI)
    )
    let displayVal = this.val
    if (Math.round(this.val) !== this.val){
      displayVal = Math.round(100*this.val)/100
    }
    if (this.val >= 1000 || this.val <= -1000){
      displayVal = this.val.toExponential()
    }
    p.strokeWeight(3/5)
    p.stroke(100)
    p.noFill()
    p.textAlign(p.CENTER, p.BOTTOM)
    p.textSize(9)
    p.text(displayVal, this.x, this.y - 4)
    p.fill(this.bgCol)
    p.stroke(this.fgCol)
    p.textAlign(p.CENTER, p.CENTER)
    p.textSize(12)
    p.text(this.id, this.x, this.y + this.radius + 13)
  }


  pair(toneObj, toneObjProperty){
    this.toneObj = toneObj
    this.toneObjProperty = toneObjProperty
  }


  set_pair_val(){
    // console.log("this.val:", this.val)
    switch(this.toneObjProperty){
      case "volume":
      // console.log("dB:", 40*Math.log(this.val))
      this.toneObj[this.toneObjProperty].value = 40*Math.log(this.val)
      break
      case "portamento":
      this.toneObj[this.toneObjProperty] = this.val
      // const key = this.toneObjProperty
      // this.toneObj.set({
      //   key: this.val
      // })
      break
      case "spread":
      this.toneObj[this.toneObjProperty] = this.val
      break
      case "detune":
      this.toneObj[this.toneObjProperty].value = this.val
      break
      case "seconds":
      this.toneObj[this.toneObjProperty] = this.val
      break
      default:
      // Er?
    }

      // this.toneObj.set({
      //   toneObjProperty: this.val
      // })
  }


  set_val(){
    // Alpha is small +ve in first quadrant,
    // approaching +PI by end of second quadrant,
    // flips to -PI in third quadrant,
    // approaching small -ve by end of fourth quadrant.
    const alpha = p.atan2(
      this.y - p.mouseY,
      p.mouseX - this.x
    )
    // Beta is -PI in fourth quadrant,
    // approaching small -ve by end of first quadrant,
    // flips to small +ve in second quadrant,
    // approaching +PI by end of third quadrant.
    let beta
    if (alpha > -p.HALF_PI){
      beta = alpha - p.HALF_PI
    }
    else {
      beta = 3*p.PI/2 + alpha
    }
    // console.log("alpha:", alpha, "beta:", beta)
    const candidateVal = p.map(
      beta,
      -p.PI, p.PI,
      this.min, this.max
    )
    // Map the candidate value to the closest gradation,
    // if a step argument was provided when constructing
    // the dial.
    if (this.step !== null){
      const ma = mu.min_argmin(
        this.gradations.map(function(g){
          return Math.abs(g - candidateVal)
        })
      )
      this.val = this.gradations[ma[1]]
    }
    else {
      this.val = candidateVal
    }

    // If a Tone.js object property has been paired with
    // this dial, update the property on the Tone.js object.
    if (this.toneObj !== undefined && this.toneObjProperty !== undefined){
      this.set_pair_val()
    }
  }


  toggle_moving(){
    this.moving = !this.moving
  }


  touch_check(){
    return p.dist(
      p.mouseX, p.mouseY, this.x, this.y
    ) <= this.radius
  }
}
