export default class Waveforms {
  constructor(_x, _y, _w, _h){
    this.x = _x
    this.y = _y
    this.w = _w
    this.h = _h

    this.arr = []
    this.movingIdx = -1
  }


  add_waveform(_url, _x, _y){
    this.arr.push(
      new Waveform(_url, _x, _y, this)
    )
  }


  draw(){
    background(220)
    // Outer rectangle
    push()
    noFill()
    stroke(100, 100, 130)
    strokeWeight(6)
    rect(this.x - 3, this.y - 3, this.w + 6, this.h + 6, 5)
    pop()

    // Waveforms
    push()
    fill(220); noStroke()
    rect(this.x, this.y, this.w, this.h)
    drawingContext.clip()
    this.arr.forEach(function(wf){
      if (wf.graphicsBuffer){
        wf.draw()
      }
    })
    pop()

    // Playhead
    if (
      Tone.Transport.seconds >= screenLRepresents &&
      Tone.Transport.seconds < screenLRepresents + screenWRepresents
    ){
      stroke(100, 100, 130)
      const x = map(
        Tone.Transport.seconds,
        screenLRepresents,
        screenLRepresents + screenWRepresents,
        this.x,
        this.x + this.w
      )
      line(
        x, this.y, x, this.y + this.h
      )
    }
  }


  move(){
    if (this.movingIdx >= 0){
      this.arr[this.movingIdx].move()
    }
  }


  playback(){
    const self = this
    Tone.Transport.scheduleRepeat(function(){
      Tone.Draw.schedule(function(){
        self.draw()
      }, Tone.now())
    }, 0.05)
    // Tone.Transport.seconds = 0
    Tone.Transport.start()
  }


  touch_check(){
    this.movingIdx = this.arr.findIndex(function(wf){
      return wf.touch_check()
    })
    return this.movingIdx
  }


  touch_end(){
    if (this.movingIdx >= 0){
      this.arr[this.movingIdx].touch_end(this.x, this.w)
      this.movingIdx = -1
    }
  }
}
