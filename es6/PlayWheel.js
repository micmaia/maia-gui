export default class PlayWheel extends Dial {
  constructor(_id, _x, _y, _radius, _min = 0, _max = 1, _val = 0.5, _step = null){
    super(_id, _x, _y, _radius, _min, _max, _val, _step)
    // Any extra properties/actions here, which could have been
    // passed into the constructor also...
    this.disabled = true
    this.playing = false

  }


  draw(){
    // Draw play/pause button.
    p.strokeWeight(3)
    p.stroke(this.fgCol)
    p.fill(this.bgCol)
    if (this.playing){
      // Draw pause button.
      p.rectMode(p.CORNER)
      p.rect(
        this.x - 2/3*this.radius, this.y - 0.5*this.radius,
        2/3*this.radius, 4/3*this.radius,
        5
      )
      p.rect(
        this.x - 2/3*this.radius, this.y - 0.5*this.radius,
        2/3*this.radius, 4/3*this.radius,
        5
      )
    }
    else {
      // Draw play button.
      p.triangle(
        this.x - 0.5*this.radius, this.y - 0.5*this.radius,
        this.x - 0.5*this.radius, this.y + 0.5*this.radius,
        this.x + 0.5*this.radius, this.y,
        5
      )
    }

    // Draw dial.
    p.strokeWeight(3)
    p.stroke(this.fgCol)
    p.fill(this.bgCol)
    p.circle(this.x, this.y, 2*this.radius)
    p.stroke(100)
    p.line(this.x, this.y, this.x, this.y + this.radius)
    p.stroke(this.fgCol)
    const prop = (this.val - this.min)/(this.max - this.min)
    p.line(
      this.x,
      this.y,
      this.x + this.radius*p.cos(p.TWO_PI*prop - p.HALF_PI),
      this.y - this.radius*p.sin(p.TWO_PI*prop - p.HALF_PI)
    )
    // Do not do display of value or id here.
    // let displayVal = this.val
    // if (Math.round(this.val) !== this.val){
    //   displayVal = Math.round(100*this.val)/100
    // }
    // if (this.val >= 1000 || this.val <= -1000){
    //   displayVal = this.val.toExponential()
    // }
    // p.strokeWeight(3/5)
    // p.stroke(100)
    // p.noFill()
    // p.textAlign(p.CENTER, p.BOTTOM)
    // p.textSize(9)
    // p.text(displayVal, this.x, this.y - 4)
    // p.fill(this.bgCol)
    // p.stroke(this.fgCol)
    // p.textAlign(p.CENTER, p.CENTER)
    // p.textSize(12)
    // p.text(this.id, this.x, this.y + this.radius + 13)
  }


  set_max(_max){
    this.max = _max
    if (this.step !== null){
      let n = Math.floor((this.max - this.min)/this.step) + 1
      this.gradations = new Array(n)
      for (let i = 0; i < n; i++){
        this.gradations[i] = this.min + this.step*i
      }
    }
  }


  toggle_disabled(){
    this.disabled = !this.disabled
  }


  toggle_play(){
    this.playing = !this.playing
  }


  touch_check(){
    // Establish general proximity to dial.
    const d = p.dist(
      p.mouseX, p.mouseY, this.x, this.y
    )
    if (d > this.radius){ return false }
    // Toggle play if it's within 3/4 of the radius.
    if (d < 0.75*this.radius){ return "toggle play" }
    // Otherwise control the outer wheel (dial).
    else { return "move dial" }
  }

}
