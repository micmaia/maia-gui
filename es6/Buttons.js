export default class Buttons {
  constructor(_buttonsStruct, _containerDimensions){
    this.buttonsStruct = _buttonsStruct
    this.keys = Object.keys(this.buttonsStruct)
    this.x = _containerDimensions.x
    this.y = _containerDimensions.y
    this.w = _containerDimensions.width
    this.h = _containerDimensions.height
  }

  draw(){
    const self = this
    self.keys.forEach(function(k, idx){
      self.buttonsStruct[k].draw(self.buttonsStruct[k].x)
    })
  }

  touch_check_helper(){
    let self = this
    // Check general area.
    // Currently removed any possibility of select menus.
    if (
      p.mouseX < self.x || p.mouseX > self.x + self.w ||
      p.mouseY < self.y || p.mouseY > self.y + self.h
    ){
      return { "click": false, "index": -1 }
    }

    // Move on to checking individual buttons.
    let buttonClick = false
    let bidx = 0
    while (bidx < self.keys.length && !buttonClick){
      // console.log("GOT INSIDE WHILE!")
      buttonClick = self.buttonsStruct[self.keys[bidx]].touch_check()
      if (buttonClick){
        return { "click": true, "index": bidx }
      }
      bidx++
    }
    return { "click": false, "index": -1 }
  }

}
