export default class Button {
  constructor(
    theLabelImg, theDisabled, theClicked, theX, theY, theWidth, theHeight
  ){
    // Workaround for JS context peculiarities.
    // var self = this;
    this.labelImg = theLabelImg
    // this.label = theLabel
    this.disabled = theDisabled
    this.clicked = theClicked
    this.x = theX
    this.y = theY
    this.w = theWidth
    this.h = theHeight
    this.buttonStroke = p.color("#17baef")
    this.buttonFill = p.color("#074f66")
    // this.labelFill = p.color(225)
    this.buttonStrokeClicked = p.color("#eebf3f")
    this.buttonFillClicked = p.color("#eebf3f")
    // this.labelFillClicked = p.color(225)
    this.buttonStrokeDisabled = p.color("#999999")
    this.buttonFillDisabled = p.color("#cccccc")
    // this.labelFillDisabled = p.color(225)

    // Possible to return something.
    // return sth;
  }

  draw(newX){
    if (newX !== undefined){
      this.x = newX
    }
    p.ellipseMode(p.CORNER)
    // p.rectMode(p.CORNER)
    if (this.disabled){
      p.stroke(this.buttonStrokeDisabled)
      p.fill(this.buttonFillDisabled)
    }
    else {
      if (this.clicked){
        p.stroke(this.buttonStrokeClicked)
        p.fill(this.buttonFillClicked)
      }
      else {
        p.stroke(this.buttonStroke)
        p.fill(this.buttonFill)
      }
    }
    p.ellipse(this.x, this.y, this.w, this.h)
    // p.rect(this.x, this.y, this.w, this.h)
    p.imageMode(p.CENTER)
    p.image(
      this.labelImg, this.x + this.w/2, this.y + this.h/2, 0.5*this.h,
      0.5*this.h
    )
  }

  onclick(){
    if (this.disabled){
      return
    }
    if (prm.printConsoleLogs) { console.log("Button \"" + this.label + "\" has been clicked!") }
    this.toggle_clicked()
    this.draw()
  }

  set_image(anImg){
    this.labelImg = anImg
  }

  toggle_clicked(){
    this.clicked = !this.clicked
  }

  toggle_disabled(){
    this.disabled = !this.disabled
  }

  touch_check(){
    if (
      p.mouseX > this.x && p.mouseX < this.x + this.w &&
      p.mouseY > this.y && p.mouseY < this.y + this.h
    ){
      // if (prm.printConsoleLogs) {
        console.log("GOT TO A TOUCH CHECK IN BUTTON!")
      // }
      this.onclick()
      return true
    }
    return false
  }

}
