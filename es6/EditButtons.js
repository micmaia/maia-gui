export default class EditButtons extends Buttons {
  constructor(_buttonsStruct, _containerDimensions){
    super(_buttonsStruct, _containerDimensions)
    // Any extra properties/actions here, which could have been
    // passed into the constructor also...
    // this.subject = subject

  }

  touch_check(){
    const self = this
    const helperResult = self.touch_check_helper()
    if (!helperResult.click) { return }
    const buttonClick = helperResult.click
    const bidx = helperResult.index

    // Must be a button click of an enabled button if we're going to do anything
    // else about it.
    if (!self.buttonsStruct[self.keys[bidx]].disabled){
      // Enforce inability to deselect a selected button.
      if (prm.grid.param.editMode == self.keys[bidx]){
        self.buttonsStruct[self.keys[bidx]].clicked = true
        self.buttonsStruct[self.keys[bidx]].draw()
      }
      // Enforce mutual exclusivity of the clicked-ness of the edit buttons.
      prm.grid.param.prevEditMode = prm.grid.param.editMode
      prm.grid.param.editMode = self.keys[bidx]
      // console.log("prm.grid.param.editMode:", prm.grid.param.editMode)
      self.keys.forEach(function(k, idx){
        // console.log("idx:", idx, "bidx:", bidx)
        if (idx !== bidx){
          self.buttonsStruct[k].clicked = false
          self.buttonsStruct[k].draw()
        }
      })

      // If this is cursor/note edit mode, we should highlight a note and the
      // nav buttons.
      if (prm.grid.param.editMode == "cursor"){
        // let notes = compObj.notes.filter(function(n){ return n.stampDelete == null })
        if (prm.grid.inner.oblongs.length == 0){
          alert("Use the pencil icon to write a note before trying to edit note properties.")
          // Could do extra logic here, or revise above blocks to not allow
          // transition to cursor mode.
          return
        }
        prm.navign.buttons.buttonsStruct["right"].toggle_disabled()
        prm.navign.buttons.buttonsStruct["left"].toggle_disabled()
        prm.navign.buttons.keys.forEach(function(k){
          prm.navign.buttons.buttonsStruct[k].toggle_clicked()
        })
        prm.grid.inner.selectedOblong = mu.choose_one(prm.grid.inner.oblongs)
        prm.grid.inner.selectedOblong.toggle_highlight()
        draw_components()
      }
      else if (prm.grid.param.prevEditMode == "cursor" && prm.grid.param.editMode !== "cursor"){
        // Turn off all highlighting.
        if (prm.grid.inner.selectedOblong !== undefined || prm.grid.inner.selectedOblong !== null){
          prm.grid.inner.selectedOblong.toggle_highlight()
        }
        prm.grid.inner.selectedOblong = null
        prm.navign.buttons.buttonsStruct["right"].toggle_disabled()
        prm.navign.buttons.buttonsStruct["left"].toggle_disabled()
        prm.navign.buttons.keys.forEach(function(k){
          prm.navign.buttons.buttonsStruct[k].toggle_clicked()
        })
        draw_components()
      }
    }
  }

}
