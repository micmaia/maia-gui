==============

Classes for building MAIA GUIs supporting various applications by Music Artificial Intelligence Algorithms, Inc.

## Local Installation

Set yourself up with a folder containing an index.html file, and containing or referencing [p5.js](https://p5js.org/) and [Tone.js](https://tonejs.github.io/) libraries. For example, see [here](https://editor.p5js.org/tomthecollins/sketches/EpUnmCBSb)

### User

**Client-side use.** Copy the classes you need from [maia-gui.js](https://bitbucket.org/micmaia/maia-gui/src/master/) to a JavaScript file where [p5.js](https://p5js.org/) and [Tone.js](https://tonejs.github.io/) are well defined, and integrate them into your GUI from there.

**Server-side use/command-line use.** This is a client-side library and is not intended to be used on the server side or via command line.


### Developer

With [Node.js](https://nodejs.org/) set up, clone the MAIA GUI repository from [here](https://bitbucket.org/micmaia/maia-gui/) and run `npm install` to acquire the dependencies. Some packages, such as Rollup, might need a general install.

Please follow these steps when making additions or changes:

1. Additions or changes to the code should be made in the es6 folder;
2. When documenting, follow the JSDoc format used therein;
3. Write unit tests below each method/function;
4. Execute `npm run compile` to convert the various components in the es6 into the corresponding components in the dist folder, and to combine them into an IIFE (called maia-gui.js, in the root of the repository);
5. Execute `jsdoc --configure .jsdoc.config.js dist` to update the documentation, which gets written to the docs folder, and check it looks good and reads well;
6. Say in step 1 you added a new file to the es6 folder called hello_world.js, then now it's time to check on your unit tests by executing `node hello_world.js` and seeing whether the `console.log()`s match your expected output;
7. Once you are satisfied with your unit tests, comment them out and paste them to test/index.js, following the chai format used therein. Execute `npm test` to verify that there are ticks everywhere and in particular that your new tests are being invoked;
8. Do the usual `git add .`, `git commit -m "Short meaningful message"`, and `git push`, and we'll see it on the other side as a pull request;
9. There should not be any need for you to edit the version in package.json;
10. Please keep any data files out of the repository by editing the .gitignore file.

## Hello-world examples

TBD

## Tests

TBD

## Contributing

TBD

## Release History

* 0.0.0 Initial release
